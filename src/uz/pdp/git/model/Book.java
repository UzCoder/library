package uz.pdp.git.model;

import java.util.UUID;

public class Book extends BaseModel{
    private String name;
    private String author;
    private int amount;

    public Book(String name, String author, int amount,UUID bookId){
        super(bookId);
        this.name = name;
        this.author = author;
        this.amount = amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public String getName() {
        return name;
    }

    public String getAuthor() {
        return author;
    }

    public int getAmount() {
        return amount;
    }
}
