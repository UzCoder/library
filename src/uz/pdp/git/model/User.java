package uz.pdp.git.model;

import java.util.UUID;

public class User extends BaseModel{
    private String name;
    private String email;
    private String password;

    public User(String name, String email, String password,UUID userId) {
        super(userId);
        this.name = name;
        this.email = email;
        this.password = password;
    }
}
