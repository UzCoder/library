package uz.pdp.git.model;

import java.util.UUID;

public abstract class BaseModel {
    protected UUID id = UUID.randomUUID();
    public BaseModel(UUID id) {
        this.id = id;
    }

    public UUID getId(){
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }
}
