package uz.pdp.git.model;

import java.util.UUID;

public class Rent extends BaseModel{
    private UUID userId;
    private int amount;

    public Rent(UUID bookId, UUID userId, int amount) {
        super(bookId);
        this.userId = userId;
        this.amount = amount;
    }
}
